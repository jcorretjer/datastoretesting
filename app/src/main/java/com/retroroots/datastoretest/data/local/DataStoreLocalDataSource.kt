package com.retroroots.datastoretest.data.local

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import com.retroroots.datastoretest.dataStore
import kotlinx.coroutines.flow.map

class DataStoreLocalDataSource (context: Context)
{
    private val dataStore = context.dataStore

    companion object
    {
        const val DATA_STORE_NAME = "NAME"

        private const val KEY_NAME = "KEY"

        val INT_KEY = intPreferencesKey(KEY_NAME)
    }

    fun getValue() = dataStore.data.map {
        it[INT_KEY] ?: 0
    }

    suspend fun updateValue(value : Int)
    {
        dataStore.edit{
            it[INT_KEY] = value
        }
    }
}