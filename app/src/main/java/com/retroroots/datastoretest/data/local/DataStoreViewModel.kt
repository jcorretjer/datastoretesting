package com.retroroots.datastoretest.data.local

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DataStoreViewModel @Inject constructor(private val dataStoreRepo: DataStoreRepo) : ViewModel()
{
    private val mutableStateFlow : MutableStateFlow<Int> = MutableStateFlow(-1)

    init
    {
        requestValue()
    }

    fun getValueFlow() : StateFlow<Int> = mutableStateFlow

    private fun requestValue()
    {
        viewModelScope.launch(Dispatchers.IO) {
            dataStoreRepo.getValue().collectLatest {
                mutableStateFlow.value = it
            }
        }
    }

    fun updateValue(value : Int)
    {
        viewModelScope.launch(Dispatchers.IO) {
            dataStoreRepo.updateValue(value)
        }
    }
}