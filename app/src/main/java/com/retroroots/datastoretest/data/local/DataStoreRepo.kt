package com.retroroots.datastoretest.data.local

import javax.inject.Inject

class DataStoreRepo @Inject constructor (private val localDataSource: DataStoreLocalDataSource)
{
    fun getValue() = localDataSource.getValue()

    suspend fun updateValue(value : Int)
    {
        localDataSource.updateValue(value)
    }
}