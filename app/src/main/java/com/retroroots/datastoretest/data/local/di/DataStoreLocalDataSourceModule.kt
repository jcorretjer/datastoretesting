package com.retroroots.datastoretest.data.local.di

import android.content.Context
import com.retroroots.datastoretest.data.local.DataStoreLocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataStoreLocalDataSourceModule
{
    @Singleton
    @Provides
    fun providesDataStoreLocalDataSource(@ApplicationContext context: Context) = DataStoreLocalDataSource(context)
}