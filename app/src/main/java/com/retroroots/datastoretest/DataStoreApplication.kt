package com.retroroots.datastoretest

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.retroroots.datastoretest.data.local.DataStoreLocalDataSource
import dagger.hilt.android.HiltAndroidApp

val Context.dataStore : DataStore<Preferences> by preferencesDataStore(name = DataStoreLocalDataSource.DATA_STORE_NAME)

@HiltAndroidApp
class DataStoreApplication : Application()
