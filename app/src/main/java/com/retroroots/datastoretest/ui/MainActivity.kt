package com.retroroots.datastoretest.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.retroroots.datastoretest.data.local.DataStoreViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity()
{
    private val dataStoreViewModel by viewModels<DataStoreViewModel>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        setContent {
            MainUI()
        }

        //dataStoreViewModel.updateValue(10)
    }

    @Preview()
    @Composable
    fun MainUI()
    {
        //As soon as the data updates this triggers the recomposition. That's just how state works.
        val value = dataStoreViewModel.getValueFlow().collectAsState()

        Column(modifier = Modifier.fillMaxWidth()) {
            Text(value.value.toString())
        }
    }
}

